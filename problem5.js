/* ==== Problem #5 ====
The car lot manager needs to find out how many cars are older than 
the year 2000. Using the array you just obtained from the previous 
problem, find out how many cars were made before the year 2000 and 
return the array of older cars and log its length.*/

import { result } from './problem4.js'

//console.log(result)

const problem5 = arrName => {
    arrName.sort((number1, number2) => number1-number2);
    //console.log(arrName)
    let requiredIndex
    for (let index = 0; index < arrName.length; index++) {
        if (arrName[index] === 2000) {
            requiredIndex = index
            break;
        }
    }
    //const slicedList = arrName.slice(index, arrName.length)
    arrName.length = requiredIndex
    //console.log(arrName)
    return arrName;
}
export const sortedListLength = problem5(result).length
console.log(sortedListLength)