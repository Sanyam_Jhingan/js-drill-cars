import { result } from "../problem6.js";

for (let index = 0; index < result.length; index++) {
    if (result[index]['car_make'] !== 'Audi' && result[index]['car_make'] !== 'BMW') {
        console.log('Test Problem6 failed!')
        console.log(result[index]['car_make'])
    } else {
        console.log('Test Problem6 passed!')
    }
}